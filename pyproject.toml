[build-system]
requires = ["poetry>=0.12"]
build-backend = "poetry.masonry.api"

[tool.poetry]
name = "openflexure-microscope-server"
version = "2.5.0"
description = "Python module, and Flask-based web API, to run the OpenFlexure Microscope."

authors = [
    "Joel Collins <j.collins@bath.ac.uk>",
    "Richard Bowman <r.w.bowman@bath.ac.uk",
    "Julian Stirling <j.stirling@bath.ac.uk>"
]

readme = 'README.md'

repository = "https://gitlab.com/openflexure/openflexure-microscope-server"
homepage = "https://www.openflexure.org"

keywords = ['raspberry pi', 'arduino', 'microscope']

license = "GPL-3.0"

exclude = ["**/node_modules/"]

packages = [
    { include = "openflexure_microscope" },
]

[tool.poetry.dependencies]
python = "^3.6"
Flask = "^1.0"
numpy = "1.18.2"
Pillow = "^7.2.0"
scipy = "1.4.1" # Exact version so we can guarantee a wheel
"RPi.GPIO" = { version = "^0.7.0", optional = true }
python-dateutil = "^2.8"
psutil = "^5.6.7" # Autostorage extension
opencv-python-headless = [
    {version = "4.1.0.25", python = "~3.7"},  # PiWheels build for RPi running Py37
    {version = "4.2.0.34", python = "^3.8"}  # Latest for Py38 systems
]
pynpm = "^0.1.2"
sangaboard = "^0.2"
expiringdict = "^1.2.1"
camera-stage-mapping = "0.1.4"
picamerax = ">=20.9.1"
pyyaml = "^5.3.1"
labthings = "1.1.0"

[tool.poetry.extras]
rpi = ["RPi.GPIO"]

[tool.poetry.scripts]
build_static = 'openflexure_microscope.install:build'

[tool.poetry.dev-dependencies]
pynpm = "^0.1.2"
sphinxcontrib-httpdomain = "^1.7"
rope = "^0.14.0"
pylint = "^2.3"
black = {version = "^18.3-alpha.0",allow-prereleases = true}
